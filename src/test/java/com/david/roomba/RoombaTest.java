package com.david.roomba;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import com.david.utils.FileImporter;
import com.david.utils.InputProcessor;

public class RoombaTest {
	
	@Test
	public void TestRoomNavigation() throws Exception {

		List<Integer> roomDimensions = new ArrayList<Integer>();		
		Set<List<Integer>> dirtPositions = new HashSet<List<Integer>>();
		List<Integer> hooverPosition = new ArrayList<Integer>();
		ArrayList<String> drivingInstructions = new ArrayList<String>();
		
		roomDimensions.add(0,5);
		roomDimensions.add(1,5);
		
		ArrayList<Integer> dirtyPosition = new ArrayList<Integer>();
		dirtyPosition.add(0,0);
		dirtyPosition.add(1,1);
		dirtPositions.add(dirtyPosition);
		
		hooverPosition.add(0,0);
		hooverPosition.add(1,0);
		
		drivingInstructions.add(0,"N");
		drivingInstructions.add(1,"S");
		drivingInstructions.add(2,"E");
		drivingInstructions.add(3,"W");
		drivingInstructions.add(4,"N");
		
		Room testRoom = new Room(roomDimensions, dirtPositions, hooverPosition);
		InitialState initState = new InitialState(testRoom, drivingInstructions);
		
		DrivingInstructionExecutor die = new DrivingInstructionExecutor(initState);
		
		die.executeDrivingInstructions();
		
		List<Integer> finalHooverPosition = die.getHooverPosition();
				
		assertThat(finalHooverPosition.get(0), equalTo(0));
		assertThat(finalHooverPosition.get(1), equalTo(1));
		
		assertThat(die.getAmountOfDirtHoovered(), equalTo(1));

	}
	
	@Test
	public void TestFileImporter() throws Exception {
		
		ArrayList<String> inputLines = new ArrayList<String>();
		inputLines.add("5 5");
		inputLines.add("1 2");
		inputLines.add("1 0");
		inputLines.add("2 2");
		inputLines.add("2 3");
		inputLines.add("NNESEESWNWW");
		
		FileImporter fileImporter = mock(FileImporter.class);
		when(fileImporter.getInput()).thenReturn(inputLines);
		
		InputProcessor inputProcessor = new InputProcessor(fileImporter);
		
		InitialState startState = inputProcessor.getInitialStateFromSource();
		
		Room room = startState.getInitialRoom();
		
		assertThat(room.getHooverPosition().get(0), equalTo(1));
		assertThat(room.getHooverPosition().get(1), equalTo(2));
		
		assertThat(room.getRoomDimensions().get(0), equalTo(5));
		assertThat(room.getRoomDimensions().get(1), equalTo(5));
		
		assertThat(startState.getDrivingInstructions().get(0), equalTo("N"));
		assertThat(startState.getDrivingInstructions().get(2), equalTo("E"));
		
	}
	
	@Test
	public void TestHooverObeysBoundsNavigation() throws Exception {
		
		//TODO: Test that the hoover does not go past the boundaries set out by the dimensions of the room	
	}
	
	@Test
	public void TestAccurateDirtCounting() throws Exception {
		
		//TODO: Test that runs the hoover over the same spot multiple times to ensure accurate dirt counting
	}

	@Test
	public void TestValidator() throws Exception {
		
		//TODO: Test that runs each of the validation methods with various inputs
		
	}
	
}
