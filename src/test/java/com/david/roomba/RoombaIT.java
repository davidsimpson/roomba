package com.david.roomba;

import java.io.IOException;
import java.util.List;

import com.david.utils.FileImporter;
import com.david.utils.InputProcessor;

import org.junit.Test;


public class RoombaIT {

	@Test
	public void runTest() {

		InitialState startState = null;

		try {
			FileImporter fi = new FileImporter("resources/input.txt");
			InputProcessor ip = new InputProcessor(fi);
			startState = ip.getInitialStateFromSource();
		} catch (IOException ioe) {
			System.out.println("Error reading from file: ");
			ioe.printStackTrace();
		} catch (Exception e) {
			System.out.println("Error processing input: ");
			e.printStackTrace();
		}

		if (startState != null) {

			DrivingInstructionExecutor die = new DrivingInstructionExecutor(
					startState);

			die.executeDrivingInstructions();

			List<Integer> finalHooverPosition = die.getHooverPosition();
			System.out.println("final position: " + finalHooverPosition.get(0)
					+ " " + finalHooverPosition.get(1));

			System.out.println("amount hoovered: "
					+ die.getAmountOfDirtHoovered());

		}

	}
}
