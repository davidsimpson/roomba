package com.david.roomba;

import java.util.ArrayList;
import java.util.List;

public class DrivingInstructionExecutor {
	
	private Room room;
	private ArrayList<String> drivingInstructions;
	private int initialDirt;
	
	public DrivingInstructionExecutor(InitialState initialState){
		room = initialState.getInitialRoom();
		drivingInstructions = initialState.getDrivingInstructions();
		initialDirt = room.getDirtPositions().size();
	}

	public void executeDrivingInstructions() {
				
		for(String instruction : drivingInstructions){
			room.move(instruction);
		}	
	}

	public int getAmountOfDirtHoovered() {
		return initialDirt - room.getDirtPositions().size();
	}

	public List<Integer> getHooverPosition() {
		return room.getHooverPosition();
	}
	
}

