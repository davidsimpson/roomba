package com.david.roomba;

import java.util.ArrayList;

/*
 * An initial state consists of a room and the set of instructions to be executed
 */

public class InitialState {
	
	private final Room initialRoom;
	private final ArrayList<String> drivingInstructions;
	
	public InitialState(Room initialRoom, ArrayList<String> drivingInstructions) {
		this.initialRoom = initialRoom;
		this.drivingInstructions = drivingInstructions;
	}

	public Room getInitialRoom() {
		return initialRoom;
	}

	public ArrayList<String> getDrivingInstructions() {
		return drivingInstructions;
	}	

}
