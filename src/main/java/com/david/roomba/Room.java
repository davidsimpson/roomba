package com.david.roomba;

import java.util.List;
import java.util.Set;

public class Room {

	private final List<Integer> roomDimensions;
	// A List seems like a strange choice here for the coordinates because it
	// will be a fixed size.
	// However, hashes for arrays are reference dependent so we can't do remove
	// operations on the set with an array.
	// TODO: We could consider refactoring this to create a position object with the
	// desired hashing bahaviours.
	private final Set<List<Integer>> dirtPositions;
	private List<Integer> hooverPosition;

	// There is no max south or west as they will always be 0;
	private int maxNorth;
	private int maxEast;

	public Room(List<Integer> roomDimensions, Set<List<Integer>> dirtPositions,
			List<Integer> hooverPosition) {

		this.roomDimensions = roomDimensions;
		this.dirtPositions = dirtPositions;
		this.hooverPosition = hooverPosition;

		// The maximum values (inclusive) for the North and East Coordinates.
		// The top right valid hoover position is [maxEast, maxNorth].
		maxNorth = roomDimensions.get(1) - 1;
		maxEast = roomDimensions.get(0) - 1;
		
	}

	public List<Integer> getRoomDimensions() {
		return roomDimensions;
	}

	public Set<List<Integer>> getDirtPositions() {
		return dirtPositions;
	}

	public List<Integer> getHooverPosition() {
		return hooverPosition;
	}

	public void move(String instruction) {

		switch (instruction) {
		case "N":
			if (hooverPosition.get(1) + 1 <= maxNorth) {
				hooverPosition.set(1, hooverPosition.get(1) + 1);
				dirtPositions.remove(hooverPosition);
			}
			break;
		case "S":
			if (hooverPosition.get(1) - 1 >= 0) {
				hooverPosition.set(1, hooverPosition.get(1) - 1);
				dirtPositions.remove(hooverPosition);
			}
			break;
		case "E":
			if (hooverPosition.get(0) + 1 <= maxEast) {
				hooverPosition.set(0, hooverPosition.get(0) + 1);
				dirtPositions.remove(hooverPosition);
			}
			break;
		case "W":
			if (hooverPosition.get(0) - 1 >= 0) {
				hooverPosition.set(0, hooverPosition.get(0) - 1);
				dirtPositions.remove(hooverPosition);
			}
			break;
		}
	}

}
