package com.david.utils;

import java.util.List;
import java.util.Set;

import com.david.roomba.Room;

// The Validator class provides static methods for validating input.  
// Validation methods return a ValidationResult object which consists of:
//	a boolean describing whether the data is valid
//	a message to help with debugging

public class Validator {

	public static ValidationResult validateRoomSize(Room room) {

		return new ValidationResult(false, "method not written yet");

	}

	public static ValidationResult validateCoordinateInput(String line) {

		String coordinateInputRegex = "^\\d+\\s\\d+$";

		if (line.matches(coordinateInputRegex)) {
			return new ValidationResult(true, "OK");
		} else {
			return new ValidationResult(
					false,
					"line: "
							+ line
							+ " does not match the required format for coordinates e.g. 5 5");
		}
	}

	public static ValidationResult validateDrivingInstructionsInput(String line) {

		String coordinateInputRegex = "^[NSEW]*$";

		if (line.matches(coordinateInputRegex)) {
			return new ValidationResult(true, "OK");
		} else {
			return new ValidationResult(
					false,
					"line: "
							+ line
							+ " does not match the required format for driving instructions e.g. NSEW");
		}
	}

	public static ValidationResult validateRoom(List<Integer> roomDimensions,
			Set<List<Integer>> dirtPositions, List<Integer> hooverPosition) {

		int maxNorth = roomDimensions.get(1) - 1;
		int maxEast = roomDimensions.get(0) - 1;

		// is the room zero in size?
		if (roomDimensions.get(0) == 0 && roomDimensions.get(1) == 0) {
			return new ValidationResult(false, "Room is of zero size");
		}

		// is the current position of the hoover valid?
		int hooverX = hooverPosition.get(0);
		int hooverY = hooverPosition.get(1);
		if ((hooverX > maxEast) || (hooverX < 0) || (hooverY > maxNorth)
				|| (hooverY < 0)) {
			return new ValidationResult(false, "Hoover is outside of room");
		}

		// are the dirt positions valid?
		for (List<Integer> dirtPosition : dirtPositions) {
			int dirtX = dirtPosition.get(0);
			int dirtY = dirtPosition.get(1);
			if ((dirtX > maxEast) || (dirtX < 0) || (dirtY > maxNorth)
					|| (dirtY < 0)) {
				return new ValidationResult(false, "Dirt is outside of room: "
						+ dirtX + " " + dirtY);
			}
		}

		return new ValidationResult(true, "OK");
	}

}
