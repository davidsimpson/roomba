package com.david.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class FileImporter {
	
	private String path;
	
	public FileImporter(String path){
		this.path = path;
	}
	
	public ArrayList<String> getInput() throws IOException {

		ArrayList<String> inputLines = new ArrayList<String>();
		File inputFile = new File(path);

		try (BufferedReader br = new BufferedReader(new FileReader(inputFile))) {
			String line;
			while ((line = br.readLine()) != null) {
				inputLines.add(line);
			}

			return inputLines;

		} catch (IOException ioe) {
			throw new IOException(
					"Exception when reading input file.  Make sure input.txt is present", ioe);
		}
	}

}
