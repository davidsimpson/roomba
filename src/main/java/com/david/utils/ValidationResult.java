package com.david.utils;
/*
 * A validation result object is returned as the result of any validation.
 * 
 * We use this to object so that a human readable message can be returned if a validation fails.
 * 
 */
public class ValidationResult {
	
	private final Boolean valid;
	private final String message;
	
	public ValidationResult(Boolean valid, String message){
		this.valid = valid;
		this.message = message;
	}
	
	public Boolean isValid(){
		return valid;
	}
	
	public String getMessage(){
		return message;
	}

}
