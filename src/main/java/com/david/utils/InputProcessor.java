package com.david.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.david.roomba.InitialState;
import com.david.roomba.Room;

public class InputProcessor {

	// TODO: This should be refactored to expect a generic "source" interface
	// which FileImporter implements. Passing a FileImporter object to this
	// constructor makes this class much easier to test as we can pass mocked
	// versions of the FileImporter
	private FileImporter fileImporter;

	public InputProcessor(FileImporter fileImporter) {
		this.fileImporter = fileImporter;
	}

	// TODO: We want to handle exceptions at the highest level because they are
	// fatal but we should refactor to use a custom exception rather than just
	// Exception
	public InitialState getInitialStateFromSource() throws IOException,
			Exception {

		// first line is room dimension
		// second line is hoover position
		// 0 or more lines of dirt
		// last line is driving instructions

		List<Integer> roomDimensions = new ArrayList<Integer>();
		List<Integer> hooverPosition = new ArrayList<Integer>();
		Set<List<Integer>> dirtPositions = new HashSet<List<Integer>>();
		ArrayList<String> drivingInstructions = null;

		FileImporter fi;

		try {
			ArrayList<String> inputLines = fileImporter.getInput();

			for (int i = 0; i < inputLines.size(); i++) {

				String line = inputLines.get(i);

				if (i == 0) {
					// It's the first line - room dimension
					ValidationResult vr = Validator
							.validateCoordinateInput(line);
					if (vr.isValid()) {
						String[] roomDimensionStrings = line.split(" ");
						roomDimensions = convertStringArrayToInt(roomDimensionStrings);
					} else {
						System.out.println("ERROR READING FIRST LINE: "
								+ vr.getMessage());
						return null;
					}

				} else if (i == 1) {
					// It's the second line - hoover position
					ValidationResult vr = Validator
							.validateCoordinateInput(line);
					if (vr.isValid()) {
						String[] hooverPositionStrings = line.split(" ");
						hooverPosition = convertStringArrayToInt(hooverPositionStrings);
					} else {
						System.out.println("ERROR READING HOOVER POSITION: "
								+ vr.getMessage());
						return null;
					}

				} else if (i == inputLines.size() - 1) {
					// It's the last line - driving instructions
					ValidationResult vrdi = Validator
							.validateDrivingInstructionsInput(line);
					if (vrdi.isValid()) {
						drivingInstructions = new ArrayList<String>(
								Arrays.asList(line.split("(?!^)")));
					} else {
						System.out
								.println("ERROR READING DRIVING INSTRUCTIONS: "
										+ vrdi.getMessage());
						return null;
					}

				} else {
					// The rest of the lines are dirt positions
					ValidationResult vrc = Validator
							.validateCoordinateInput(line);
					if (vrc.isValid()) {
						String[] dirtPositionStrings = line.split(" ");
						dirtPositions
								.add(convertStringArrayToInt(dirtPositionStrings));
					} else {
						System.out.println("ERROR READING DIRT POSITION: "
								+ vrc.getMessage());
						return null;
					}
				}
			}

		} catch (IOException ioe) {
			throw new IOException("importing file failed", ioe);
		}

		// Check the validity of the room we're about to create
		ValidationResult validRoom = Validator.validateRoom(roomDimensions,
				dirtPositions, hooverPosition);

		if (validRoom.isValid()) {
			return new InitialState(new Room(roomDimensions, dirtPositions,
					hooverPosition), drivingInstructions);
		} else {
			System.out.println("Input room is invalid: "
					+ validRoom.getMessage());
			return null;
		}
	}

	// Converts String coordinates into integer coordinates
	private static List<Integer> convertStringArrayToInt(String[] inputArray)
			throws Exception {
		try {
			List<Integer> outputList = new ArrayList<Integer>();
			for (int i = 0; i < inputArray.length; i++) {
				outputList.add(i, Integer.parseInt(inputArray[i]));
			}
			return outputList;
		} catch (Exception e) {
			throw new Exception(
					"Exception converting coordinates to integers.  Offending input: "
							+ inputArray.toString(), e);
		}
	}

}
