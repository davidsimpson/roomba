# Roomba #
----------------------

This project builds with maven so you should be able to build it with a simple

    mvn install

To make this even easier I've checked in a built version so you can just download and run that if building it is proving a bother.